#!/bin/bash

#temp script: TBD better.

function usage {
 echo "`basename $0` test {date} {preferable_wd}"
 echo "         test = test/prod"
 echo "         date = YYYYMMDD"
 echo "         preferable_wd  = parent dir of <file>.raw"
}

[ -z $1 ] && usage && exit 1

if [ x$1 == "xprod" ]; then
    os_edition='Base'
    OSEDITION=""
else
    os_edition='Test'
    OSEDITION="TEST"
fi
[ -z $2 ] && KOJIIMGDATE=`date "+%Y%m%d"` || KOJIIMGDATE=$2
[ -z $3 ] && cwd=`pwd` || cwd=$3 && cd $3
echo "$0 working inside ${cwd}"

UPLDAYDATE=`echo ${KOJIIMGDATE} | cut -c 1-4 | tr -d '\n' && echo -n "-"`
UPLDAYDATE="${UPLDAYDATE}`echo ${KOJIIMGDATE} | cut -c 5-6 | tr -d '\n' && echo -n \"-\"`"
UPLDAYDATE="${UPLDAYDATE}`echo ${KOJIIMGDATE} | cut -c 7-8 | tr -d '\n'`"
FORMAT="raw"
hw_firmware_type="bios"
hw_machine_type="pc"

ARCHS="x86_64"
os_distro="RHEL"
# PROD RHEL images must not be taken into account for cleanup, they are never public and are long-lived
if [ "${OSEDITION}" == "TEST" ]; then
    centos_test_cleanup="true"
else
    centos_test_cleanup="false"
fi

for ARCH in ${ARCHS}; do
    daydate=$UPLDAYDATE
    release_date="${daydate}T13:13:13"
    upstream_provider="linux.support@cern.ch"
    [ $ARCH == "i686" ] && FARCH="i386" || FARCH=$ARCH
    img=`ls *raw`
    echo "Inspecting ${img}:"
    # Upstream images from Red Hat will not have a oz log file, but the file is conveniently named with this information :)
    version=`ls *raw |cut -d\- -f1 | sed 's/rhel//'`
    if [[ -z "$version" ]]; then
      echo "Unable to figure out the version, something is very wrong."
      exit 1
    fi
    os_distro_major=${version:0:1}
    if [ ${#version} -gt 1 ]; then
      os_distro_minor=`echo $version | cut -d. -f2`
    else
      os_distro_minor=0
    fi
    if [ X${OSEDITION} == "X" ]; then
        image_name="$os_distro$os_distro_major - ${ARCH} [$daydate]"
    else
        image_name="$os_distro$os_distro_major ${OSEDITION} - ${ARCH} [$daydate]"
    fi

    # Show what we're working with, which might be useful for debugging
    # Protip: if you need to inspect the image, you can use this command:
    #    LIBGUESTFS_BACKEND=direct guestfish --ro -a ${img}
    # in the resulting shell, run "run" to start the VM, then "mount /dev/sda1"
    # (or whatever) to mount the filesystem and then you can "cat" whatever you want.
    PARTITIONS=$(LIBGUESTFS_BACKEND=direct virt-filesystems -a ${img} --long --uuid --no-title)
    echo "Partition table:"
    echo "Name      Type       VFS Label Size Parent UUID"
    printf "$PARTITIONS\n"
    echo "-----------------------------------------------"

    if [[ $(printf "$PARTITIONS\n" | wc -l) -eq 1 ]]; then
        # If there's only one partition, it's got to be the root
        rootfs_uuid=$(printf "$PARTITIONS\n" | awk '{print $7}')
    else
        # RHEL images use 'root', we define the label to be 'ROOT'. Use a case insensitive grep
        rootfs_uuid=$(printf "$PARTITIONS\n" | grep -i ROOT | awk '{print $7}')
    fi
    if [[ -z "$rootfs_uuid" ]]; then
        echo "Unable to find rootfs UUID, something is very wrong."
        exit 1
    fi
    echo "rootfs_uuid=${rootfs_uuid}"

    openstack image create -f json --container-format bare --disk-format ${FORMAT} \
        --property os="LINUX" \
        --property hypervisor_type="qemu" \
        --property os_distro="$os_distro" \
        --property os_distro_major="$os_distro_major" \
        --property os_distro_minor="$os_distro_minor" \
        --property release_date="$release_date" \
        --property os_edition="$os_edition" \
        --property gitops="enable" \
        --property centos_test_cleanup="$centos_test_cleanup" \
        --property architecture="${ARCH}" \
        --property hw_architecture="${ARCH}" \
        --property custom_name="$image_name" \
        --property upstream_provider="$upstream_provider" \
        --property name="$image_name" \
        --property rootfs_uuid="$rootfs_uuid" \
        --property hw_firmware_type="$hw_firmware_type" \
        --property hw_machine_type="$hw_machine_type" \
        $KSFILE \
        --file $img \
        "$image_name" | tee upload.json
    ## Checking the openstack command's exit code (PIPESTATUS[0]),
    OS_EXIT_CODE=${PIPESTATUS[0]}
    echo "PIPESTATUS[0] is: " ${OS_EXIT_CODE}
    if [ ${OS_EXIT_CODE} -ne 0 ]; then
            echo "ERROR: openstack command failed. Check stderr."
            exit 1
    fi
done
