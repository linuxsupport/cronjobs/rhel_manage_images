#!/bin/bash

TRIGGER_REPO="linuxsupport/testing/image-ci"
LINUXCI_API_TOKEN=${LINUXCI_APITOKEN}
OS_IMG_ID=$1
TRIGGER_JOB=$2

waitFor() {
  MAX_ATTEMPTS=$1
  shift
  POLLING_INTERVAL=$1
  shift
  ATTEMPT_COUNTER=0

  while [[ $ATTEMPT_COUNTER -lt $MAX_ATTEMPTS ]]; do
    ATTEMPT_COUNTER=$(($ATTEMPT_COUNTER+1))
    # Run the user's command
    $@
    RET=$?

    case $RET in
      0)
        # 0 for success
        return 0
        ;;
      2)
        # 2 for canceled
        return 2
        ;;
      3)
        # 3 for failed
        return 3
        ;;
      1|4)
        # 1 for unknown state, 4 for pending/still running
        ;;
    esac

    sleep $POLLING_INTERVAL
  done

  echo "Max attempts reached."
  # 1 for timeout
  return 1
}

checkPipeline() {
  # Use https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
  PROJECT=$1
  PROJECT_PATH_ENC=$(echo $PROJECT | sed 's#/#%2F#g' )
  PIPELINE_ID=$2

  # Wait for pipeline to finish
  POLLING_STATUS="$(curl -sL --max-time 10 --header "PRIVATE-TOKEN: ${LINUXCI_API_TOKEN}" \
    https://gitlab.cern.ch/api/v4/projects/${PROJECT_PATH_ENC}/pipelines/${PIPELINE_ID} | jq -r .status)"

  case $POLLING_STATUS in
    "created"|"running"|"pending")
      echo "Still running, check https://gitlab.cern.ch/${PROJECT}/pipelines/${PIPELINE_ID}"
      return 4
      ;;
    "success")
      return 0
      ;;
    "canceled")
      return 2
      ;;
    "failed")
      return 3
      ;;
    *)
      echo "Unknown state: ${POLLING_STATUS}."
      return 1
      ;;
  esac
}

PIPELINE_ID=$(curl -sL --request POST \
  --header "PRIVATE-TOKEN: $LINUXCI_API_TOKEN" \
  --header "Content-Type: application/json" \
  --data "{ \"ref\": \"master\", \"variables\": [ {\"key\": \"IMAGE\", \"value\": \"${OS_IMG_ID}\"}, {\"key\": \"PROJECT_VIRTUAL\", \"value\": \"${OS_PROJECT_NAME}\"}, {\"key\": \"TEST_VIRTUAL\", \"value\": \"True\"},{\"key\": \"TEST_PHYSICAL\", \"value\": \"False\"}, {\"key\": \"TEST_UNMANAGED\", \"value\": \"True\"}, {\"key\": \"TEST_PUPPET\", \"value\": \"True\"}, {\"key\": \"${TRIGGER_JOB}\", \"value\": \"True\"} ] }" \
  "https://gitlab.cern.ch/api/v4/projects/linuxsupport%2Ftesting%2Fimage-ci/pipeline" | jq -r .id)

### The original script doesn't fail:
#if [ "$PIPELINE_ID" == "null" ]; then
#  echo "Could not create a pipeline for https://gitlab.cern.ch/${TRIGGER_REPO}  :("
#  exit 0
#fi

### In case of any failure, return code is != 0 and message is logged to stderr. Now the runner(Python script) can catch the error!
if [ "$PIPELINE_ID" == "null" ]; then
  echo "Could not create a pipeline for https://gitlab.cern.ch/${TRIGGER_REPO} :(" >&2
  exit 1
fi

### In case of any failure, return code is != 0 and message is logged to stderr. Now the runner(Python script) can catch the error!

# 360 attempts and 60 sec poll interval equals to 6 hours.
# Usual pipelines are about 4 hours long: https://gitlab.cern.ch/linuxsupport/rpms/openafs/pipelines
# Adding 2 extra hours for possible waiting times in CI and Koji
waitFor 360 60 checkPipeline $TRIGGER_REPO $PIPELINE_ID
RETURN_CODE=$?
if [ $RETURN_CODE -ne 0 ]; then
  case $RETURN_CODE in
    1)
      echo "Pipeline reached timeout: https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID}" >&2
      ;;
    2)
      echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} was canceled." >&2
      ;;
    3)
      echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} was failed." >&2
      ;;
    4)
      echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} got an unknown status." >&2
      ;;
  esac
  exit 1
fi
