#!/usr/bin/python3

import datetime
import json
import os
import re
from ssl import SSLError
import requests
from requests import exceptions
import sys
import hashlib
import collections
import pycdlib
import time
import subprocess

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import ast
from keystoneauth1 import session
from keystoneauth1.extras.kerberos import MappedKerberos
from glanceclient import Client

ADMIN_EMAIL = os.getenv('ADMIN_EMAIL')
USER_EMAIL = os.getenv('USER_EMAIL')
OFFLINE_TOKEN = os.getenv('RHSM_OFFLINE_TOKEN')

"""
	Main method for managing RHEL images (ISO and QEMU).
	Errors are send to ADMIN_EMAIL.
	Successful managing is sent to USER_EMAIL.
"""
def main():

	## Main variables
	OS_AUTH_URL = "https://keystone.cern.ch/v3"
	OS_PROTOCOL = "kerberos"
	OS_MUTUAL_AUTH = "disabled"
	OS_IDENTITY_PROVIDER = "sssd"
	OS_PROJECT_DOMAIN_ID = "default"
	OS_USER_DOMAIN_NAME = "default"
	OS_PROJECT_NAME = os.getenv('OS_PROJECT_NAME')
	CSETS = os.getenv('CSETS')

	## Setting ENVs, needed by any shell script (executed through this Python script) runs command `openstack`
	os.environ["OS_AUTH_URL"] = OS_AUTH_URL
	os.environ["OS_PROJECT_DOMAIN_ID"] = OS_PROJECT_DOMAIN_ID
	os.environ["OS_USER_DOMAIN_NAME"] = OS_USER_DOMAIN_NAME

	## Connecting to RHEL
	if OFFLINE_TOKEN is None:
		subject = "RHEL manage images - Token error"
		body = "ERROR: Offline_token variable needs to be passed, exiting."
		send_email(ADMIN_EMAIL, subject, body)
		print(body)
		sys.exit(0)
	auth_token = get_rhel_auth(OFFLINE_TOKEN)

	## Connecting to OpenStack
	# For connecting to OpenStack you must first get kerberos credentials (It is done through CLI)
	os_imgs_as_dict = []
	auth = MappedKerberos(auth_url=OS_AUTH_URL,
						protocol=OS_PROTOCOL,
						mutual_auth=OS_MUTUAL_AUTH,
						identity_provider=OS_IDENTITY_PROVIDER,
						project_domain_id=OS_PROJECT_DOMAIN_ID,
						project_name=OS_PROJECT_NAME)
	sess = session.Session(auth=auth)
	glance = Client('2', session=sess)
	os_images = glance.images.list()
	for img in os_images:
		os_imgs_as_dict.append(ast.literal_eval(str(img).strip()))
	flt_os_imgs = []
	for img in os_imgs_as_dict:
		if "custom_name" in img.keys():
			if "TEST" not in img["custom_name"] and ("RHEL7" in img["custom_name"] or "RHEL8" in img["custom_name"] or "RHEL9" in img["custom_name"]):
				flt_os_imgs.append(img)
	print(f"Successfully connected to OS project '{OS_PROJECT_NAME}'. All available images are: {str(len(os_imgs_as_dict))}")

    ## Getting list of available RHEL images according to cset
	failed_csets = []
	success_isos = []
	failed_isos = {}
	success_qemus = []
	failed_qemus = {}
	cset_list = [cset.strip() for cset in CSETS.split(",")]
	for cset in cset_list:
		time.sleep(3)
		url = (f"https://api.access.redhat.com/management/v1/images/cset/{cset}?limit=100")

		retry = 0
		max_retry = 3
		content = ""
		while retry < max_retry:
			attempts = retry + 1
			headers = { 'Authorization': f'Bearer {auth_token}' }
			result = requests.get(url, headers=headers)
			if result.status_code == 200:
				content = json.loads(result.content)
				print(f"'{cset}' CSET content retrieved successfully from Red Hat after {attempts} attempt(s).")
				break
			else:
				if attempts == max_retry:
					failed_csets.append(cset)
					print(f"ERROR: Getting cset content of {cset} failed on attempt no. {attempts}, which is the last attempt. Proceeding to the next cset.")
					retry += 1
					break
				else:
					retry += 1
					sleep = 5
					print(f"ERROR: Getting cset content of {cset} failed on attempt no. {attempts}. Sleep for {sleep} sec and then request new auth_token.")
					time.sleep(sleep)
					print("Now I will ask for a new auth_token")
					auth_token = get_rhel_auth(OFFLINE_TOKEN)
		if retry == max_retry:
			continue

		content = json.loads(result.content)
		isos_of_interest = []
		qemus_of_interest = []
		for item in content['body']:
			# Getting all dvd.iso (iso images) items
			if 'dvd.iso' in item['filename']:
				isos_of_interest.append(item)
			# Getting all .qcow2 (qemu images) items
			if 'qcow2' in item['filename'] and 'x86_64' in item['filename'] and re.findall("7.[0-9]", item['filename']):
				qemus_of_interest.append(item)

		if len(isos_of_interest) > 0:
			iso_managing(isos_of_interest, auth_token, success_isos, failed_isos)
		if len(qemus_of_interest) > 0:
			qemu_managing(glance, qemus_of_interest, auth_token, success_qemus, failed_qemus, flt_os_imgs)

	## Management of RHEL images is complete. Logging and sending report(s) through email(s).
	manage_emails(failed_csets, success_isos, failed_isos, success_qemus, failed_qemus)

	sys.exit(0)


"""
	iso_managing() retrieves all ISO images of interest based on what is available from Red Hat,
	compares them with what is available at Ceph FS in CERN and initiates all processes
	for download, extraction and addition to AIMS (if applicable).
	It adds items to a list of successfully managed ISO image(s) a dictionary of unsuccessfully managed one(s).
	ARG:	a list of available isos, RHEL authorization token, lists of successful and failed isos
	RETURN:	- (items are added to success_isos and failed_isos. These lists are used as class variables)
"""
def iso_managing(isos_of_interest, auth_token, success_isos, failed_isos):

	# iso_handling() Initiates all the processes (download, extract, put-to-AIMS) if an iso of interest doesn't exist in CephFS
	def iso_handling(auth_token, item, arch_rel_dir):

		downl_status = download_rhel_img(auth_token, item, arch_rel_dir)
		if downl_status[0]:
			if extract_iso(downl_status[1], arch_rel_dir):
				latest_version_tracking(arch_rel_dir)						# Update 'latest' symlink and 'version' file in case of a new download
				if arch == "x86_64" or arch == "aarch64":
					exec_add_to_aims_stage(item)
				else:
					print("ppc64le architecture is not allowed in aims!")
				success_isos.append(item)
			else:
				failed_isos[item['filename']] = "No extraction."
		else:
			failed_isos[item['filename']] = downl_status[1]


	major = 0
	arch = ""
	for item in isos_of_interest:
		item_release = re.findall("\d+\.\d+", item['filename'])[0]
		major = item_release.split(".")[0]
		arch = item['arch']
		major_rel_dir = os.path.join("/rhel", major)
		if os.path.exists(major_rel_dir):
			minor_rel_dir = os.path.join(major_rel_dir, item_release)
			if os.path.exists(minor_rel_dir):
				arch_rel_dir = os.path.join(minor_rel_dir, arch)
				if os.path.exists(arch_rel_dir):
					print(f"{item['filename']} is already downloaded.")
					latest_version_tracking(arch_rel_dir)					# Check 'latest' symlink and 'version' file.
				else:
					iso_handling(auth_token, item, arch_rel_dir)
			else:
				arch_rel_dir = os.path.join(minor_rel_dir, arch)
				iso_handling(auth_token, item, arch_rel_dir)
		else:
			arch_rel_dir = os.path.join(major_rel_dir, item_release, arch)
			iso_handling(auth_token, item, arch_rel_dir)


"""
	qemu_managing() retrieves all QEMU images of interest based on what is available from Red Hat,
	compares them with what is available at OpenStack in CERN and initiates all processes
	for downloading, re-building, upload2openstack, testing and ma(r)king as production OS images.
	It adds items to a list of successfully managed QEMU image(s) a dictionary of unsuccessfully managed one(s).
	ARG:	a list of available isos, RHEL authorization token, lists of successful and failed isos
	RETURN:	- (items are added to success_qemus and failed_qemus. These lists are used as class variables)
"""
def qemu_managing(glance_client, qemus_of_interest, auth_token, success_qemus, failed_qemus, qemus_in_os):

	## Pre-Stage: Find the latest available upstream qemu
	latest_qemu_release = '6.0'
	latest_qemu = {}
	for qemu in qemus_of_interest:
		qemu_release = re.findall("\d+\.\d+", qemu['filename'])[0]
		if qemu_release > latest_qemu_release:
			latest_qemu_release = qemu_release
			latest_qemu = qemu
	major = latest_qemu_release.split(".")[0]
	minor = latest_qemu_release.split(".")[1]
	operatingsystem = f"RHEL{major}"
	release_date = datetime.datetime.strptime(latest_qemu['datePublished'].split('T')[0], '%Y-%m-%d')

	# Check if the latest available upstream qemu exists in OpenStack
	for qemu_in_os in qemus_in_os:
		print(f"Comparing {operatingsystem} released on {str(release_date.strftime('%Y-%m-%d'))} - {latest_qemu_release} \tVS\t {qemu_in_os['custom_name']} - {qemu_in_os['os_distro_major']}.{qemu_in_os['os_distro_minor']}")
		if major == qemu_in_os["os_distro_major"] and minor ==  qemu_in_os["os_distro_minor"] and release_date.strftime('%Y-%m-%d') in qemu_in_os['custom_name']:
			print(f"A production {operatingsystem} image with the release date of {release_date.strftime('%Y-%m-%d')} already exists in OpenStack.")
			return

	## Download stage (aka download_upstream_rhelX)
	print("\tDownload stage")
	downl_dir = os.path.join(os.getcwd(), operatingsystem)
	downl_status = download_rhel_img(auth_token, latest_qemu, downl_dir)

	## Build stage (aka build_rhelX)
	if downl_status[0]:
		date_on_filename = release_date.strftime('%Y%m%d')
		print(f"\tBuild stage.\tcurrent_fp:\t{downl_status[1]}")
		build_status = exec_build_stage(latest_qemu_release, date_on_filename, downl_status[1])
	else:
		failed_qemus[latest_qemu['filename']] = downl_status[1]
		return

	## Upload stage (aka upload_rhelX_test)
	if build_status[0]:
		print(f"\tUpload stage.\tRaw file is:\t{build_status[1]}")
		upload_status = exec_upload_stage(date_on_filename, build_status[1])
	else:
		failed_qemus[latest_qemu['filename']] = build_status[1]
		return

	# intermediate stage: Variable(s) for next stages
	if upload_status[0]:
		upload_json_dict = {}
		with open(upload_status[1]) as upload_json:
			upload_json_dict = json.load(upload_json)
	else:
		failed_qemus[latest_qemu['filename']] = upload_status[1]
		return

	## Tests stage (aka share_rhelX_test)
	print("\tTests stage")
	tests_status = exec_tests_stage(upload_json_dict)

	## Prod stage (python-ized)
	if tests_status[0]:
		print("\tProd stage")
		new_img_name = upload_json_dict["name"].replace("TEST ", "")
		new_img_custom_name = upload_json_dict["properties"]["custom_name"].replace("TEST ", "")
		if os.getenv('OS_PROJECT_NAME') == "IT Linux Support - CI VMs":
			glance_client.images.update(upload_json_dict['id'], name=new_img_name, custom_name=new_img_custom_name, os_edition="Base", visibility="community")
		else:
			glance_client.images.update(upload_json_dict['id'], name=new_img_name, custom_name=new_img_custom_name, os_edition="Base")
		latest_qemu['upload_info'] = upload_json_dict
		success_qemus.append(latest_qemu)
		print(f"QEMU image {latest_qemu['filename']} was managed successfully!")
	else:
		failed_qemus[latest_qemu['filename']] = tests_status[1]
		return


"""
	download_rhel_img() downloads an image from Red Hat (e.g. iso, qcow) and verifies it through checksum.
	ARG:	the whole dictionary with info of the iso to be downloaded, auth token and path for downloading
	RETURN: tuple = (Bool: True for success, False for fail,
					String: ISO filepath for success, Fail report for fail)
"""
def download_rhel_img(auth_token, iso_dict, downl_dirpath):

	filename = iso_dict['filename']
	release_date = datetime.datetime.strptime(iso_dict['datePublished'].split('T')[0], '%Y-%m-%d')
	url = iso_dict['downloadHref']
	local_auth_token = auth_token

	## Download img request
	retry = 0
	max_retry = 3
	fail_reason = {}
	while retry < max_retry:
		attempts = retry + 1
		headers = { 'Authorization': f'Bearer {local_auth_token}' }
		try:
			result = requests.get(url, headers=headers, stream=True)
		except SSLError:
			fail_reason[attempts] = "SSLError"
		if attempts not in fail_reason.keys() and result.status_code == 200:
			print(f"Downloading {filename}, published on {release_date.strftime('%Y-%m-%d')}")
			break
		else:
			if attempts not in fail_reason.keys():
				fail_reason[attempts] = "!= 200 return code"
			if attempts == max_retry:
				print(f"ERROR: {fail_reason[attempts]}, on attempt no. {attempts} to download {filename}, which is the last attempt. Proceeding to the next image.")
				return (False, str(fail_reason))
			else:
				print(f"ERROR: {fail_reason[attempts]}, on attempt no. {attempts} to download {filename}. Sleep for 5 sec and then retry with new auth.")
				retry += 1
				time.sleep(5)
				local_auth_token = get_rhel_auth(OFFLINE_TOKEN)

	## Downloading img
	os.makedirs(downl_dirpath)
	retry = 0
	max_retry = 3
	fail_reason = {}
	while retry < max_retry:
		attempts = retry + 1
		start = time.time()
		iso_fp = os.path.join(downl_dirpath, filename)
		with open(iso_fp, 'wb') as f:
			try:
				for chunk in result.iter_content(chunk_size=1024):
					if chunk:
						f.write(chunk)
			except exceptions.StreamConsumedError:
				f.close()
				fail_reason[attempts] = "StreamConsumedError error"
		end = time.time()
		downl_time = round(((end - start)/60), 2)
		print(f"Downloading procedure for {filename} lasted {downl_time} min.")
		## Verify the downloaded iso
		sha256_hash = hashlib.sha256()
		with open(iso_fp, "rb") as bytefile:
			for byteblock in iter(lambda: bytefile.read(4096), b""):
				sha256_hash.update(byteblock)
			h256 = sha256_hash.hexdigest()
		if h256 == iso_dict['checksum']:
			print(f"{filename} is successfully downloaded after {attempts} attempt(s).")
			break
		else:
			if attempts in fail_reason.keys():
				if attempts == max_retry:
					print(f"ERROR: {fail_reason[attempts]}, on attempt no. {attempts} to download {filename}, which is the last attempt. Proceeding to the next image.")
					return (False, str(fail_reason))
				else:
					print(f"ERROR: {fail_reason[attempts]}, on attempt no. {attempts} to download {filename}. Sleep for 5 sec and then retry.")
					time.sleep(5)
					retry += 1
			else:
				fail_reason[attempts] = "Not fully downloaded"
				if attempts == max_retry:
					print(f"ERROR: {fail_reason[attempts]}, on attempt no. {attempts} to download {filename}, which is the last attempt. Proceeding to the next image.")
					return (False, str(fail_reason))
				else:
					print(f"ERROR: {fail_reason[attempts]}, on attempt no. {attempts} to download {filename}. Sleep for 5 sec and then retry.")
					time.sleep(5)
					retry += 1

	return (True, iso_fp)


"""
	extract_iso() extracts the contents of an iso file in a specified directory.
	ARGS:	The filepaths of an iso file and that of the desirable directory to extract the contents.
	RETURN:	Boolean (True for success, False for fail)
"""
def extract_iso(iso_fp, extract_dir):

	pathname    = 'rr_path'
	start_path  = "/"
	filename = os.path.basename(iso_fp)
	print(f"Extracting:\t{filename}")

	iso = pycdlib.PyCdlib()
	iso.open(iso_fp)
	root_entry = iso.get_record(**{pathname: start_path})

	dirs = collections.deque([root_entry])
	while dirs:
		dir_record = dirs.popleft()
		ident_to_here = iso.full_path_from_dirrecord(dir_record, rockridge=pathname == 'rr_path')
		relname = ident_to_here[len(start_path):]
		if relname and relname[0] == '/':
			relname = relname[1:]

		if dir_record.is_dir():
			if relname != '':
				os.makedirs(os.path.join(extract_dir, relname))
			child_lister = iso.list_children(**{pathname: ident_to_here})
			for child in child_lister:
				if child is None or child.is_dot() or child.is_dotdot():
					continue
				dirs.append(child)
		else:
			if dir_record.is_symlink():
				fullpath = os.path.join(extract_dir, relname)
				local_dir = os.path.dirname(fullpath)
				local_link_name = os.path.basename(fullpath)
				old_dir = os.getcwd()
				os.chdir(local_dir)
				os.symlink(dir_record.rock_ridge.symlink_path(), local_link_name)
				os.chdir(old_dir)
			else:
				iso.get_file_from_iso(os.path.join(extract_dir, relname), **{pathname: ident_to_here})
	iso.close()

	## Verify extraction contents
	verify = False
	not_found = ""
	dirs_expected = ["AppStream", "BaseOS",  "images"]
	files_expected = ["EULA", "extra_files.json", "GPL", "media.repo", "RPM-GPG-KEY-redhat-beta", "RPM-GPG-KEY-redhat-release"]
	for d in dirs_expected:
		dir_to_check = os.path.join(extract_dir, d)
		if os.path.isdir(dir_to_check):
			verify = True
		else:
			not_found = dir_to_check
			verify = False
			break
	if verify:
		for f in files_expected:
			file_to_check = os.path.join(extract_dir, f)
			if os.path.isfile(file_to_check):
				verify = True
			else:
				not_found = file_to_check
				verify = False
				break

	if re.findall("\d+\.\d+", filename)[0].split(".")[0] not in ["8", "9"] or "dvd" not in filename:
		print("Major release not [8, 9] or dvd not in filename.")
		verify = True

	if verify:
		print(f"All contents of {filename} are extracted successfully.")
		return verify
	else:
		print(f"ERROR: {filename} is successfully downloaded, but NOT successfully extracted. {not_found} file/dir was NOT found!")
		return verify


"""
	exec_add_to_aims_stage() prepares the arguments for and runs the shell script iso_add_to_aims.sh.
	iso_add_to_aims.sh executes aims2client command to add the recently downloaded image in AIMS.
	ARGS:	The dictionary of the iso that defines the downloaded image.
	RETURN: - (Exits if there is an error... TODO: modify and make it like the rest exec_ functions)
"""
def exec_add_to_aims_stage(iso_dict):

	arch = iso_dict['arch']

	item_release = re.findall("\d+\.\d+", iso_dict['filename'])[0]
	name = f"RHEL_{item_release.replace('.', '_')}_{arch.upper()}"

	major = item_release.split(".")[0]
	minor = item_release.split(".")[1]
	description = f"RHEL {major} SERVER UPDATE {minor} FOR {arch.upper()}"

	pxe_path = os.path.join("/rhel", major, item_release, arch, "images/pxeboot")

	nomad_task = os.getenv('NOMAD_TASK_NAME')
	project_status = nomad_task.split("_")[0]
	if project_status == "prod":
		aims_dest = ""
	else:
		aims_dest = "--testserver"

	result = subprocess.run(["/root/iso_add_to_aims.sh", name, arch, description, pxe_path, aims_dest], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
	if result.returncode == 0:
		print(f"{iso_dict['filename']} added in AIMS as {name}")
	else:
		subject = "RHEL manage images - Shell script to AIMS error"
		body = f"ERROR: iso_add_to_aims.sh did NOT run successfully due to:\n{result.stderr}"
		send_email(ADMIN_EMAIL, subject, body)
		print(body)
		sys.exit(0)


"""
	exec_build_stage() prepares the arguments for and runs the shell script qemu_convertimage.sh.
	qemu_convertimage.sh is used to apply necessary fixes to upstream RedHat images, for use at CERN.
	ARGS:	The dictionary of the iso that defines the downloaded image, date in specific format and
			filepath of the downloaded QEMU image.
	RETURN: tuple = (Bool: True for success, False for fail,
					String: filepath of .raw image if success, Fail report if fail)
"""
def exec_build_stage(latest_qemu_release, date_on_filename, current_fp):

	revision = "1"
	new_filename = f"rhel{latest_qemu_release}-cloud-{date_on_filename}-{revision}.x86_64.qcow2"
	new_fp = os.path.join(os.path.dirname(current_fp), new_filename)
	os.rename(current_fp, new_fp)

	result = subprocess.run(["/root/qemu_convertimage.sh", new_fp], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
	if result.returncode == 0:
		raw_fp = new_fp.replace("qcow2", "raw")
		if os.path.exists(raw_fp):
			print(f"Build stage executed successfully. Output file is:\t{raw_fp}")
			return (True, raw_fp)
		else:
			build_fail_report = f"ERROR: qemu_convertimage.sh did run successfully, BUT the produced file does NOT exist. Logs:\n{result.stdout}"
			print(build_fail_report)
			return (False, build_fail_report)
	else:
		build_fail_report = f"ERROR: qemu_convertimage.sh did NOT run successfully due to:\n{result.stderr}"
		print(build_fail_report)
		return (False, build_fail_report)


"""
	exec_upload_stage() prepares the arguments for and runs the shell script qemu_upload2openstack.sh.
	qemu_upload2openstack.sh prepares the .raw image and uploads it to OS (predefined OS_PROJECT)
	qemu_upload2openstack.sh produces / returns a report json file called: upload.json
	ARGS:	(convenient) filename date, filepath of .raw image file
	RETURN: tuple = (Bool: True for success, False for fail,
					String: filepath of upload.json if success, Fail report if fail)
"""
def exec_upload_stage(date_on_filename, raw_fp):

	raw_dir = os.path.dirname(raw_fp)

	result = subprocess.run(["/root/qemu_upload2openstack.sh", "test", date_on_filename, raw_dir], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
	if result.returncode == 0:
		upload_json_fp = os.path.join(raw_dir, "upload.json")
		if os.path.exists(upload_json_fp):
			print(f"Upload stage executed successfully. Output file is:\t{upload_json_fp}")
			return (True, upload_json_fp)
		else:
			upload_fail_report = f"ERROR: qemu_upload2openstack.sh did run successfully, BUT the produced file does NOT exist. Logs:\n{result.stdout}"
			print(upload_fail_report)
			return (False, upload_fail_report)
	else:
		upload_fail_report = f"ERROR: qemu_upload2openstack.sh did NOT run successfully due to:\n{result.stderr}"
		print(upload_fail_report)
		return (False, upload_fail_report)


"""
	exec_tests_stage() prepares the arguments for and runs the shell script qemu_test_os_img.sh.
	ARGS:	The dictionary of the QEMU image that is uploaded to OS.
	RETURN: tuple = (Bool: True for success, False for fail,
					String: shell script's stdout of if success, Fail report if fail)
"""
def exec_tests_stage(upload_json_dict):

	trigger_job = f"TEST_OSRH{upload_json_dict['properties']['os_distro_major']}"

	result = subprocess.run(["/root/qemu_test_os_img.sh", upload_json_dict['id'], trigger_job], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
	if result.returncode == 0:
		stdout_split = result.stdout.split(" ")
		pipeline_url = "PIPELINE URL NOT RETRIEVED"
		if len(stdout_split) > 3:
			pipeline_url = stdout_split[3]
		print(f"Tests stage executed successfully. Check the image-ci pipeline here:\t{pipeline_url}")
		return (True, result.stdout)
	else:
		tests_fail_report = f"ERROR: qemu_test_os_img.sh did NOT run successfully due to:\n{result.stderr}"
		print(tests_fail_report)
		return (False, tests_fail_report)


""""
	latest_version_tracking() function uses info from the most recently downloaded or checked iso image
	and compares with the latest recorded download image.
	It makes sure that the latest (and thus highest) release version is recorded in the 'version' file
	and its directory is pointed by 'latest' symlink.
	latest == highest version. most recently downloaded != latest
	ARG:	The directory of the most recently checked or downloaded release
	RET:	-
"""
def latest_version_tracking(current_arch_rel_dir):
																		# e.g. /rhel/8/8.7/x86_64	// arch_rel_dir
	current_version_dir = os.path.dirname(current_arch_rel_dir)			# e.g. /rhel/8/8.7
	current_version_release = os.path.basename(current_version_dir)		# e.g. 8.7
	symlink_dir = os.path.dirname(current_version_dir)					# e.g. /rhel/8
	symlink_file = os.path.join(symlink_dir, "latest")					# e.g. /rhel/8/latest	// symlink
	latest_version_file = os.path.join(symlink_dir, "version")			# e.g. /rhel/8/version 	// this file holds the latest version

	# Check 'version' file
	if os.path.exists(latest_version_file):
		version_content = open(latest_version_file).readline().rstrip()
		if current_version_release > version_content:
			with open(latest_version_file, "w") as f:
				f.write(current_version_release)
	else:
		with open(latest_version_file, "w") as f:
			f.write(current_version_release)

	# Check 'latest' symlink
	if os.path.islink(symlink_file):
		symlink_points_to_dir = os.path.realpath(symlink_file)
		symlink_points_to_ver = os.path.basename(symlink_points_to_dir)
		if current_version_release > symlink_points_to_ver:
			os.remove(symlink_file)
			os.symlink(current_version_release, symlink_file)
	else:
		os.symlink(current_version_release, symlink_file)

	## Fixing sync between 'version' and 'latest'. It happens in case one of them is missing.
	version_current_content = open(latest_version_file).readline().rstrip()
	symlink_current_pointing = os.path.basename(os.path.realpath(symlink_file))
	if version_current_content != symlink_current_pointing:
		print(f"Current version tracking system is not in sync: {version_current_content} VS {symlink_current_pointing}\nFixing it...")
		highest_arch_rel_dir = os.path.join(symlink_dir, max(version_current_content, symlink_current_pointing), "x86_64")
		latest_version_tracking(highest_arch_rel_dir)


"""
	get_rhel_auth() gets authorization based on OFFLINE_TOKEN and generates an access token
	In case of failure (after max_retry attempts) an email is sent to the admin and the Python scripts exits.
	ARG:	offline token
	RETURN:	access token
"""
def get_rhel_auth(offline_token):

	url = 'https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token'
	data = { 'grant_type': 'refresh_token', 'client_id': 'rhsm-api', 'refresh_token': offline_token }

	retry = 0
	max_retry = 3
	while retry < max_retry:
		attempts = retry + 1
		result = requests.post(url, data=data)
		if result.status_code == 200:
			auth_token = json.loads(result.content)['access_token']
			print(f"Successful RHEL authorization after {attempts} attempt(s).")
			break
		else:
			retry += 1
			sleep = 5
			print(f"Auth failed, sleeping for {sleep} sec.")
			time.sleep(sleep)
			print("Retry auth...")

	if retry == max_retry:
		subject = "RHEL manage images - Auth error"
		body = f"ERROR: Unable to auth after {attempts} attempts, exiting."
		send_email(ADMIN_EMAIL, subject, body)
		print(body)
		sys.exit(0)

	return auth_token


"""
	manage_emails() gets all lists and dictionaries with images and csets that failed for any reason and images that succeeded.
	It filters them and sends the corresponding emails to admin(s) and user(s)
	ARG:	all success and failure reports as lists and dictionaries. (CSETS, QEMU and ISO images)
	RETURN:	-
"""
def manage_emails(failed_csets, success_isos, failed_isos, success_qemus, failed_qemus):

	dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

	if len(failed_csets) != 0 or len(failed_isos) != 0 or len(failed_qemus) != 0:
		fail_email(failed_csets, failed_isos, failed_qemus)
	else:
		print(f"rhel_manage_images.py ran successfully on {dt}")
		if len(success_isos) == 0:
			print("No new RHEL ISO images found.")
		if len(success_qemus) == 0:
			print("No new RHEL QEMU images found.")
	if len(success_isos) > 0:
		success_iso_email(success_isos)
	if len(success_qemus) > 0:
		success_qemu_email(success_qemus)


"""
	fail_email() sends email to the ADMIN to inform which CSET, QEMU image(s) and ISO image(s) failed.
	ARGS:	list with failed cset(s), 2 dictionaries with the corresponding failed ISO and QEMU image(s)
"""
def fail_email(failed_csets, failed_isos, failed_qemus):

	if len(failed_csets) > 0:
		subject1 = "RHEL manage images - CSET(s) error"
		body1 = "ERROR: Check if the following CSET(s) have a problem as the(ir) content was not retrieved:\n"
		for fcset in failed_csets: body1 += fcset + "\n"
		print(body1)
		send_email(ADMIN_EMAIL, subject1, body1)

	if len(failed_isos) > 0:
		subject2 = "RHEL manage images - ISO image(s) error"
		body2 = "ERROR: Downloading the following image(s) was unsuccessful after all possible attempts:\n"
		for fiso in failed_isos: body2 += fiso + "\t" + failed_isos[fiso] + "\n"
		body2 += "\nWARNING: Failed download attempts may create directories in CEPH FS with unusable RHEL iso content.\n"
		body2 += "ACTIONS:\n\t- Inspect these directories to verify the unsuccessful downloads."
		body2 += "\n\t- Delete these directories, if you want the script to try download the corresponding iso images in the next run."
		print(body2)
		send_email(ADMIN_EMAIL, subject2, body2)

	if len(failed_qemus) > 0:
		subject3 = "RHEL manage images - QEMU image(s) error"
		body3 = "ERROR: Managing the following image(s) was unsuccessful:\n"
		for fqemu in failed_qemus: body3 += fqemu + "\t" + failed_qemus[fqemu] + "\n"
		body3 += f"\nWARNING: Fails during managing qemu image(s) may add image(s) in OpenStack project {os.environ.get('OS_PROJECT_NAME')} that are not usable.\n"
		body3 += "ACTIONS:\n\t- Inspect any unsuccessful management procedures and delete the possibly dangerous image(s)."
		print(body3)
		send_email(ADMIN_EMAIL, subject3, body3)


"""
	success_iso_email() sends email to the USER to inform which ISO image(s) succeeded.
	ARGS:	Dictionary with the corresponding successful ISO image(s).
"""
def success_iso_email(success_isos):

	isos_by_release = {}

	for iso in success_isos:
		minor_release = re.findall("\d+\.\d+", iso['filename'])[0]
		arch = iso['arch']
		if minor_release in isos_by_release:
			isos_by_release[minor_release].append(iso)
		else:
			isos_by_release[minor_release] = [iso]

	lxsoft_parent_path = "http://linuxsoft.cern.ch/enterprise/rhel/server"
	for release in isos_by_release:
		archs_to_email = "("
		aims_to_email = ""
		aims_names = []
		major = ""
		for i in range(0, len(isos_by_release[release])):
			item_release = re.findall("\d+\.\d+", isos_by_release[release][i]["filename"])[0]
			major = item_release.split(".")[0]
			arch = isos_by_release[release][i]["arch"]
			if i == len(isos_by_release[release]) - 1:
				archs_to_email += arch + ")"
			else:
				archs_to_email += arch + ", "

			if arch == "x86_64" or arch == "aarch64":
				aims_name = f"RHEL_{item_release.replace('.', '_')}_{arch.upper()}"
				aims_names.append(aims_name)
				if len(aims_names) > 1:
					aims_to_email += " or " + arch
				else:
					aims_to_email += arch

		email_to = USER_EMAIL			# default: success emails are sent to users
		if major in ["8", "9"]:
			email_to = ADMIN_EMAIL		# differentiate: When major is 8 or 9, send only to admins.
		subject = f"RHEL {release} {archs_to_email} is now available"
		body = f"Hello {email_to},\n\n"
		lxsoft_release_path = os.path.join(lxsoft_parent_path, major, item_release)
		body += f"Today RHEL {release} {archs_to_email} was released and is now available for use at CERN: {lxsoft_release_path}\n\n"
		if len(aims_names) != 0:
			body += f"You may install {aims_to_email} of this version of RHEL by utilizing the corresponding AIMS2 target:\n"
			for an in aims_names:
				body += an + "\n"
		body += "\nRegards,\nCERN Linux team"
		print(f"Sending email to {email_to} with subject: {subject}")
		send_email(email_to, subject, body)


"""
	success_qemu_email() sends email to the USER to inform which QEMU image(s) succeeded.
	ARGS:	Dictionary with the corresponding successful QEMU image(s).
"""
def success_qemu_email(success_qemus):

	for qemu in success_qemus:
		major = qemu['upload_info']['properties']['os_distro_major']
		minor = qemu['upload_info']['properties']['os_distro_minor']
		name = qemu['upload_info']['name'].replace('TEST ', '')
		qemu_id = qemu['upload_info']['id']
		subject = f"New RHEL {major}.{minor} image available"
		body = "Dear RHEL users,\n\n"
		body += f"Today a new RHEL {major}.{minor} image ({name}) is available. The image uuid is: {qemu_id}\n\n"
		body += f"You can use this image with:\n\nopenstack server create --image {qemu_id} ...\n\n"
		body += f"or alternatively from aiadm.cern.ch with\n\nai-bs -i {qemu_id} ...\n\n"
		body += "Best regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)"
		send_email(USER_EMAIL, subject, body)


"""
	send_email() works as function-template for sending email(s)
	ARGS:	receiver of the email, the subject of the email, the body of the email,	email sender
"""
def send_email(email_to, subject, body, email_from='linux.support@cern.ch'):

	server = smtplib.SMTP('cernmx.cern.ch')
	msg = MIMEMultipart()
	msg['Subject'] = subject
	msg['From'] = email_from
	msg['To'] = email_to
	msg.add_header('reply-to', 'noreply.Linux.Support@cern.ch')
	body = MIMEText(f"{body}", _subtype='plain')
	msg.attach(body)

	try:
		server.sendmail(email_from, email_to, msg.as_string())
		time.sleep(2)
	except:
		print(f"failed to send email to {email_to}, continuing...")



if __name__ == '__main__':
	main()
