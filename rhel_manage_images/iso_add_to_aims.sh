#!/bin/bash

NAME=$1
ARCH=$2
DESC=$3
PXE_PATH=$4
AIMS_DEST=$5

echo $LINUXCI_PWD | kinit $LINUXCI_USER

/usr/bin/aims2client ${AIMS_DEST} addimg \
    --name "${NAME}" \
    --arch "${ARCH}" \
    --description "${DESC}" \
    --vmlinuz "${PXE_PATH}/vmlinuz" \
    --initrd "${PXE_PATH}/initrd.img" \
    --egroup "${ADMIN_EMAIL}" \
    --noprogress \
    --force \
    --uefi
