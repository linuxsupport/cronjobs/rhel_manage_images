#!/bin/bash

# This script is used to apply neccessary fixes to upstream
# RedHat images, for use at CERN

function usage {
 echo "`basename $0` image"
}

[ -z $1 ] && usage && exit 1
IMGFILE=$1
echo "Converting $IMGFILE ..."
IMGFILEOUT=${IMGFILE/qcow2/raw}

TMPIMAGE=$(mktemp -d)
LIBGUESTFS_BACKEND=direct virt-copy-out -a $IMGFILE /var/lib/rpm $TMPIMAGE
IMAGE_VER=$(rpm -q krb5-libs --qf="%{version}-%{release}" --dbpath $TMPIMAGE/rpm)
if [[ $IMAGE_VER == *"el7"* ]]; then
  OS=7
elif [[ $IMAGE_VER == *"el8"* ]]; then
  OS=8
elif [[ $IMAGE_VER == *"el9"* ]]; then
  OS=9
else
  echo "Unsupported OS release ($IMAGE_VER)"
  exit 1
fi
echo "Image is for RHEL ${OS}."

# Apparently this is no longer needed?
# echo "Checking if bootstrap repo needs updating"
# REPO="http://linuxsoft.cern.ch/internal/bootstrap/rhel${OS}/x86_64"
# REPO_VER=$(repoquery --repoid bootstrap --repofrompath=bootstrap,$REPO --qf="%{version}-%{release}" --latest-limit 1 krb5-libs)
# if [ "$IMAGE_VER" != "$REPO_VER" ]; then
#   echo "Error: Bootstrap repo needs updating before continuing."
#   echo "Without proceeding with this step, puppet managed RHEL hosts will be unable to bootstrap"
#   echo "The image contains krb5-libs version $IMAGE_VER, however the the packages krb5-workstation, krb5-libs and libkadm5 exist in $REPO with version $REPO_VER. These packages need to be updated to $IMAGE_VER"
#   exit 1
# fi

echo "Ensuring that root can ssh (in lieu of 'cloud-user')"
LIBGUESTFS_BACKEND=direct virt-copy-out -a $IMGFILE /etc/cloud/cloud.cfg $TMPIMAGE
sed -i 's|^disable_root: .*|disable_root: 0|' $TMPIMAGE/cloud.cfg
sed -i 's|name: cloud-user|name: root|' $TMPIMAGE/cloud.cfg
sed -i '/gecos:/d' $TMPIMAGE/cloud.cfg $TMPIMAGE/cloud.cfg
sed -i '/groups:/d' $TMPIMAGE/cloud.cfg $TMPIMAGE/cloud.cfg
sed -i '/sudo:/d' $TMPIMAGE/cloud.cfg $TMPIMAGE/cloud.cfg
LIBGUESTFS_BACKEND=direct virt-copy-in -a $IMGFILE $TMPIMAGE/cloud.cfg /etc/cloud

echo "Defining .repo configuration for linuxsoft mirror"
REPOFILE="https://linux.web.cern.ch/rhel/repofiles/rhel${OS}.repo"

if [[ $OS -eq 9 ]]; then
  LIBGUESTFS_BACKEND=direct virt-customize --run-command 'yum --repofrompath cern,http://linuxsoft.cern.ch/internal/repos/cern9el-stable/x86_64/os/ --nogpgcheck -y install cern-release' -a $IMGFILE
else
  if [[ $OS -eq 7 ]]; then
    CERNURL="http://linuxsoft.cern.ch/cern/centos/${OS}/cern/x86_64/"
    curl -o $TMPIMAGE/RPM-GPG-KEY-cern https://linuxsoft.cern.ch/internal/repos/RPM-GPG-KEY-cern
    GPGKEYCERN="file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern"
  else
    CERNURL="http://linuxsoft.cern.ch/cern/centos/s${OS}/CERN/x86_64/"
  fi
  curl -o $TMPIMAGE/RPM-GPG-KEY-kojiv2 https://linuxsoft.cern.ch/internal/repos/RPM-GPG-KEY-kojiv2
  LIBGUESTFS_BACKEND=direct virt-copy-in -a $IMGFILE $TMPIMAGE/RPM-GPG-KEY-* /etc/pki/rpm-gpg
  cat > $TMPIMAGE/CERN.repo <<EOF
[CERN]
name=CERN
baseurl=${CERNURL}
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
       ${GPGKEYCERN}
EOF
fi

curl -o $TMPIMAGE/rhel.repo $REPOFILE
LIBGUESTFS_BACKEND=direct virt-copy-in -a $IMGFILE $TMPIMAGE/*.repo /etc/yum.repos.d
if [[ $OS -eq 9 ]]; then
  LIBGUESTFS_BACKEND=direct virt-customize --install glibc-langpack-en -a $IMGFILE
fi
# Note - if this step is ommited, VMs spawned from this image will not
# configure ipv4 correctly. Reference: INC2246166
echo "Configuring cloud-init to not generate network configuration"
echo "Configuring timezone to be Europe/Zurich"
echo "Configuring NetworkManager to work correctly for CERN dhcpv6"
LIBGUESTFS_BACKEND=direct guestfish -a $IMGFILE -i <<_EOF_
        touch /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg
        write /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg "network: {config: disabled}"
        touch /etc/cloud/cloud.cfg.d/99-settimezone.cfg
        write /etc/cloud/cloud.cfg.d/99-settimezone.cfg "timezone: Europe/Zurich"
        touch /etc/NetworkManager/conf.d/99-dhcpv6-duid.conf
        write /etc/NetworkManager/conf.d/99-dhcpv6-duid.conf "[connection]\nipv6.dhcp-duid=ll"
_EOF_

echo "Converting qcow2 format to raw for CERN openstack"
qemu-img convert -f qcow2 -O raw $IMGFILE $IMGFILEOUT
