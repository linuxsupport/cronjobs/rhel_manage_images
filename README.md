# manage_rhel_images

This repository holds files and scripts for automating the management of RHEL images (ISO and QEMU).

The code is scheduled to run every day within a container in Nomad.

The main Python script [rhel_manage_images.py](https://gitlab.cern.ch/linuxsupport/cronjobs/rhel_manage_images/-/blob/qemu/rhel_manage_images/rhel_manage_images.py) manages 2 kind of RHEL images: ISO and QEMU.

Workflow
- The script connects with Red Hat and gets updates for the available RHEL images

- For RHEL ISO images:
  - The available RHEL ISO images are compared with what is available at CERN (specific directory in Ceph FS)
  - If there is a new ISO image the following actions are:
     - Download
     - Extract contents.
     - Creates information for the latest RHEL major version.
        - A symlink named 'latest' that points to the dir of the latest version: /parent_dir/\<major\>/\<major\>.\<minor\>
        - A text file named 'version' that stores the latest version "\<major\>.\<minor\>"
        - Both the 'latest' symlink and the 'version' file are stored in /parent_dir/\<major\>/ dir of each major RHEL version.
     - Add to AIMS (if applicable).
     - Inform USER(s) by email.

- For RHEL QEMU images:
  - The available RHEL QEMU images are compared with what is available at CERN's OpenStack (project 'IT Linux Support - CI VMs').
  - If there is a new QEMU image the following actions are:
     - Download
     - Rebuild
     - Upload to OpenStack (marked as 'TEST').
     - Test the image using [Image CI](https://gitlab.cern.ch/linuxsupport/testing/image-ci/) repository / pipeline.
     - Mark the image production ready.
     - Inform USER(s) by email.
