job "${PREFIX}_rhel_manage_images" {
  datacenters = ["*"]
  
  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_rhel_manage_images" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/rhel_manage_images/rhel_manage_images:${CI_COMMIT_SHORT_SHA}"

      logging {
        config {
          tag = "${PREFIX}_rhel_manage_images"
        }
      }
      volumes = [ "$RHEL_MOUNT:/rhel" ]
    }
    env {
      RHSM_OFFLINE_TOKEN = "$RHSM_OFFLINE_TOKEN"
      IMAGECI_USER = "$IMAGECI_USER"
      IMAGECI_PWD = "$IMAGECI_PWD"
      LINUXCI_USER = "$LINUXCI_USER"
      LINUXCI_PWD = "$LINUXCI_PWD"
      LINUXCI_APITOKEN = "$LINUXCI_APITOKEN"
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      USER_EMAIL = "$USER_EMAIL"
      CSETS = "$CSETS"
      OS_PROJECT_NAME = "$OS_PROJECT_NAME"
      OS_USERNAME="$IMAGECI_USER"
      OS_PASSWORD="$IMAGECI_PWD"
      TAG = "${PREFIX}_rhel_manage_images"
    }

    resources {
      cpu = 3000 # Mhz
      memory = 4096 # MB
    }

  }
}
